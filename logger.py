import logging
import os
import time


class UTCFormatter(logging.Formatter):
    converter = time.gmtime


def configure_logger(debug=False):
    for logger_name in ["uvicorn.error", "uvicorn.access", "uvicorn"]:
        logger = logging.getLogger(logger_name)
        logger.setLevel(logging.INFO)
        if debug:
            logger.setLevel(logging.DEBUG)
        if len(logger.handlers) > 0:
            formatter = UTCFormatter(
                "%(asctime)s.%(msecs)03dZ | PID: " + str(os.getpid()).zfill(5) + " | %(levelname)s | %(message)s",
                "%Y-%m-%dT%H:%M:%S",
            )  # RFC3339, e.g 2021-10-20T13:08:48.927Z | ...
            logger.handlers[0].setFormatter(formatter)
    return logger
