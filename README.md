# Service Helpers

This is a collection of useful helpers that can be used by FastAPI based  web services.

## Overview

### Logger

Configure the uvicorn logger by running

```python
from service_helpers.logger import configure_logger

configure_logger()
```

This will add a timestamp and a process id to each line of the log, e.g.

```shell
2021-10-20T13:15:35.630Z | PID: 00011 | INFO | 172.17.0.1:65532 - "GET /ok HTTP/1.1" 200
2021-10-20T13:15:35.631Z | PID: 00008 | INFO | 172.17.0.1:65534 - "GET /ok HTTP/1.1" 200
```

## How to use

Use the project as a submodule by running

```shell
git submodule add https://gitlab.com/empaia/integration/service-helpers.git service_helpers
```

## Development

### Setup

```shell
poetry install
poetry shell
```

### Tests

```shell
isort .
black .
pycodestyle *.py tests
pylint *.py tests
pytest tests --maxfail=1
```
