import os
import time

import docker
import pytest
import requests


def build_test_service():
    client = docker.from_env()
    test_dir = os.path.dirname(os.path.abspath(__file__))
    client.images.build(path=test_dir, tag="test-service")


def run_test_service(workers=4):
    client = docker.from_env()
    test_dir = os.path.dirname(os.path.abspath(__file__))
    test_service_dir = os.path.join(test_dir, "test_service")
    helpers_dir = os.path.join(os.path.dirname(test_dir))
    container = client.containers.run(
        "test-service",
        name="test-service",
        remove=True,
        detach=True,
        environment={"WEB_CONCURRENCY": workers, "PYTHONDONTWRITEBYTECODE": 1},
        ports={"8765": "8765"},
        volumes={
            test_service_dir: {"bind": "/code/app", "mode": "rw"},
            helpers_dir: {
                "bind": "/code/app/service_helpers",
                "mode": "rw",
            },
        },
    )
    return container


def wait_for_service():
    service_ready = False
    while not service_ready:
        try:
            requests.get("http://localhost:8765/ok")
            service_ready = True
        except requests.exceptions.ConnectionError:
            time.sleep(0.1)


@pytest.fixture(scope="session", autouse=True)
def start_service(request):
    build_test_service()
    service = run_test_service()
    wait_for_service()

    def force_removal():
        service.remove(force=True)

    request.addfinalizer(force_removal)
