from fastapi import FastAPI
from service_helpers.logger import configure_logger  # pylint: disable=E0401

configure_logger()
app = FastAPI()


@app.get("/ok")
async def _():
    return {"status": "ok"}


@app.get("/exception")
async def _():
    0 / 0  # pylint: disable=W0104
