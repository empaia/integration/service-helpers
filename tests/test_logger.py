import asyncio
from datetime import datetime

import aiohttp
import docker
import pytest
import requests


def get_test_service_logs(timestamps=False):
    client = docker.from_env()
    container = client.containers.get("test-service")
    return container.logs(timestamps=timestamps).decode("utf-8")


def test_ok():
    r = requests.get("http://localhost:8765/ok")
    assert r.status_code == 200
    logs = get_test_service_logs()
    assert '"GET /ok HTTP/1.1" 200' in logs
    # check timestamps in logs
    now = datetime.utcnow()
    for line in iter(logs.splitlines()[2:]):
        log_time = datetime.strptime(line.split(".")[0], "%Y-%m-%dT%H:%M:%S")
        assert (now - log_time).total_seconds() < 5


def test_ok_docker_timestamps():
    r = requests.get("http://localhost:8765/ok")
    assert r.status_code == 200
    logs = get_test_service_logs(timestamps=True)
    assert '"GET /ok HTTP/1.1" 200' in logs
    # check timestamps in logs
    now = datetime.utcnow()
    for line in iter(logs.splitlines()[2:]):
        log_time = datetime.strptime(line.split(".")[0], "%Y-%m-%dT%H:%M:%S")
        assert (now - log_time).total_seconds() < 5


def test_exception():
    r = requests.get("http://localhost:8765/exception")
    assert r.status_code == 500
    logs = get_test_service_logs()
    assert '"GET /exception HTTP/1.1" 500' in logs
    assert "ERROR" in logs
    assert "Traceback" in logs
    assert "ZeroDivisionError" in logs
    # check error line has timestamp
    now = datetime.utcnow()
    for line in iter(logs.splitlines()):
        if "ERROR" in line:
            log_time = datetime.strptime(line.split(".")[0], "%Y-%m-%dT%H:%M:%S")
            assert (now - log_time).total_seconds() < 5


@pytest.mark.asyncio
async def test_async_requests(event_loop):
    event_loop.set_debug(True)
    urls = []
    for _ in range(100):
        urls.append("http://localhost:8765/ok")
    tasks = []

    async def fetch(client, url):
        async with client.get(url) as response:
            assert response.status == 200
            return await response.read()

    async with aiohttp.ClientSession() as session:
        for url in urls:
            tasks.append(fetch(session, url))
        await asyncio.gather(*tasks)
    logs = get_test_service_logs()
    worker_request_count = {}
    for line in iter(logs.splitlines()[2:]):
        if "INFO" in line:
            pid_pos = line.find("PID: ")
            pid = int(line[pid_pos + 5 : pid_pos + 11])
            worker_request_count[pid] = True
    assert len(worker_request_count) == 4
